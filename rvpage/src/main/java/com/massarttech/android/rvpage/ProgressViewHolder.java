package com.massarttech.android.rvpage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ProgressViewHolder extends RecyclerView.ViewHolder {
    public static final int PROGRESS_VIEW_TYPE = 0x786;

    public ProgressViewHolder(@NonNull View itemView) {
        super(itemView);
    }

    public ProgressViewHolder(@NonNull Context context, @NonNull ViewGroup parent) {
        super(LayoutInflater.from(context).inflate(R.layout.rv_page_item_progress, parent, false));
    }
}

package com.massarttech.android.rvpage;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

public class PageRecyclerView extends RecyclerView {
    @Nullable
    private PageLoadListener pageLoadListener;
    private int page = 0;
    private int totalPages = Integer.MAX_VALUE;

    public PageRecyclerView(@NonNull Context context) {
        super(context);
    }

    public PageRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        if (attrs != null) {
            handleAttrs(attrs);

        }
    }

    public PageRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (attrs != null) {
            handleAttrs(attrs);

        }
    }

    private void handleAttrs(@NonNull AttributeSet attributeSet) {
        TypedArray values = getContext().obtainStyledAttributes(attributeSet, R.styleable.PageRecyclerView);
        page = values.getInt(R.styleable.PageRecyclerView_rv_initial_page, 0);
        values.recycle();
    }

    public void setPageLoadListener(@NonNull PageLoadListener pageLoadListener) {
        this.pageLoadListener = pageLoadListener;
        this.addOnScrollListener(new LastItemListener());
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }


    @FunctionalInterface
    public interface PageLoadListener {
        void loadPage(int nextPage);
    }

    public class LastItemListener extends RecyclerView.OnScrollListener {
        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            RecyclerView.Adapter adapter = recyclerView.getAdapter();

            if (adapter != null && layoutManager != null && layoutManager.getChildCount() > 0) {
                int indexOfLastItemViewVisible = layoutManager.getChildCount() - 1;
                View lastItemViewVisible = layoutManager.getChildAt(indexOfLastItemViewVisible);
                if (lastItemViewVisible != null) {
                    int adapterPosition = layoutManager.getPosition(lastItemViewVisible);
                    boolean isLastItemVisible = (adapterPosition == adapter.getItemCount() - 1);
                    // check
                    if (isLastItemVisible && pageLoadListener != null && page < totalPages) {
                        pageLoadListener.loadPage(page++);
                    }
                }


            }
        }
    }
}

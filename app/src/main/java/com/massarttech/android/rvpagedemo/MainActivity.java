package com.massarttech.android.rvpagedemo;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.common.collect.Lists;
import com.massarttech.android.rvpage.PageRecyclerView;
import com.massarttech.android.rvpage.ProgressViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {
    private List<String> countries = Arrays.asList("Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final List<List<String>> splitLists = Lists.partition(countries, 20);

        PageRecyclerView pageRecyclerView = findViewById(R.id.rvPage);
        pageRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        pageRecyclerView.setTotalPages(splitLists.size());
        final CountryAdapter adapter = new CountryAdapter(this, splitLists.get(0));
        pageRecyclerView.setAdapter(adapter);

        pageRecyclerView.setPageLoadListener(new PageRecyclerView.PageLoadListener() {
            @Override
            public void loadPage(int page) {
                List<String> more = splitLists.get(page);
                adapter.addMore(more);
            }
        });

    }


    class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryHolder> {
        private final Context context;
        private final List<String> countries;

        CountryAdapter(@NonNull Context context, @NonNull List<String> countries) {
            this.context = context;
            this.countries = new ArrayList<>(countries);
        }

        public void addMore(List<String> countries) {
            final int previousSize = this.countries.size();
            this.countries.addAll(countries);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    notifyItemRangeInserted(previousSize + 1, CountryAdapter.this.countries.size());

                }
            }, 2000);
        }

        @NonNull
        @Override
        public CountryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            if (viewType == ProgressViewHolder.PROGRESS_VIEW_TYPE) {
                return new CountryHolder(parent);
            }
            return new CountryHolder(LayoutInflater.from(context)
                    .inflate(R.layout.item_country, parent, false));

        }

        @Override
        public int getItemViewType(int position) {
            if (position == countries.size()) {
                return ProgressViewHolder.PROGRESS_VIEW_TYPE;
            }
            return super.getItemViewType(position);
        }

        @Override
        public void onBindViewHolder(@NonNull CountryHolder holder, int position) {
            if (position < countries.size()) {
                holder.nameTextView.setText(countries.get(position));
            }

        }

        @Override
        public int getItemCount() {
            return countries.size() + 1;
        }

        class CountryHolder extends ProgressViewHolder {
            private TextView nameTextView;

            CountryHolder(@NonNull ViewGroup viewGroup) {
                super(viewGroup.getContext(), viewGroup);
            }

            CountryHolder(@NonNull View itemView) {
                super(itemView);
                nameTextView = itemView.findViewById(R.id.nameTextView);
            }
        }

    }
}
